--[[
 Parse NHK radio ondemand URL.

 Copyright © 2019 caplas

 Authors: caplas <rad164@teapot-lab.space>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
--]]

lazily_loaded = false
dkjson        = nil

function lazy_load()
  if lazily_loaded then return nil end
  dkjson = require("dkjson")
  lazily_loaded = true
end

function probe()
  return vlc.access == "https"
  and string.match( vlc.path, "^www%.nhk%.or%.jp/" )
  and (
    string.match( vlc.path, "/radioondemand/json/%w+/bangumi" )
    or
    string.match( vlc.path, "/radio/ondemand/detail.html%?p=" )
  )
end

function parse_json()
  local string = ""
  while true do
    local line = vlc.readline()
    if line then
      string = string .. line
    else
      break
    end
  end

  local obj, _, err = dkjson.decode(string)
  if err then
    vlc.msg.err(string.format("NHK: Failed to parse JSON %s, %s", vlc.path, err))
    vlc.msg.dbg(string.format("data: %s", string))
    return
  end

  local arturl = obj.main.site_logo
  local artist = obj.main.program_name

  local pl = {}
  for _, detail in ipairs(obj.main.detail_list) do
    for _, file in ipairs(detail.file_list) do
      local item = { arturl = arturl; }
      item["name"] = file.file_title or file.seq

      item["path"] = file.file_name
      item["artist"] = artist
      item["description"] = ""
      if file.file_title_sub then
        item["description"] = item["description"]  .. " " .. file.file_title_sub
      end
      if detail.headline_sub then
        item["description"] = item["description"]  .. " " .. detail.headline_sub
      end
      item["album"] = detail.headline or artist
      item["url"] = file.share_url
      local _, _, date = string.find( file.aa_vinfo4, "^(.*)T.*_" )
      if date then
        item["date"] = date
      end
      table.insert(pl, item)
    end
  end

  return pl
end

function forward_to_json()
  local _, _, site, corner = string.find( vlc.path, "p=(.*)_(.*)")
  if site and corner then
    local url = string.format("https://www.nhk.or.jp/radioondemand/json/%s/bangumi_%s_%s.json", site, site, corner)
    return { { path = url } }
  else
    vlc.msg.err("Failed to parse the path "..vlc.path)
    return
  end
end

function parse()
  lazy_load()
  if string.match( vlc.path, ".json$" ) then
    return parse_json()
  else
    return forward_to_json()
  end
end
