set -e

if [ -n "$XDG_DATA_HOME" ] ; then
  DEST="$XDG_DATA_HOME/vlc"
elif [ -d "$HOME/Library/Application Support/org.videolan.vlc/" ] ; then
  DEST="$HOME/Library/Application Support/org.videolan.vlc"
else
  echo "Could not find destination."
  exit 1
fi
DEST="$DEST/lua"

for d in playlist sd ; do
  [ -d "$DEST/$d" ] || mkdir -p "$DEST/$d"
  cp -v $d/*.lua "$DEST/$d"
done
